//Get active link path
function get_link_path(){
  var link = location.pathname.split('/');
  var link_updated = [];
  var link_path = "";

  for(var i = 0; i < link.length; i++) {
    if(link[i]) {
      link_updated.push(link[i]);
    }
  }

  link_path = link_updated.join("/");
  return link_path;
}

// Set active navigation item
$(function() {
  link_path = get_link_path();

  // Set selected value corresponding to active page
  $("[data-behaviour=trigger-main-nav] option").each(function(){
    if($(this).val() === ("/" + link_path + "/")){
      $(this).prop("selected", true)
    }
  });

  if(link_path.length == 0) {
      $('.page-link').first().addClass('active');
  } else {
    $('.page-link[href^="/' + link_path + '"]').addClass('active');
  }
});

// Navigate to page on change of navigation select
$(document).on("change", "[data-behaviour=trigger-main-nav]", function(){
  sel = this.options[this.selectedIndex].value;
  window.location.href = sel;
});
