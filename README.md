# Pandosearch developer documentation

This is the [Pandosearch developer documentation](https://developer.pandosearch.com/) source code repository.

Documentation is built using [MkDocs](https://www.mkdocs.org/) and published via
GitLab Pages.

A push to the `master` branch will automatically trigger a CI/CD pipeline build
and will update the live version of the documentation. See
[.gitlab-ci.yml](./.gitlab-ci.yml) for more information.

The latest published documentation version can be found at
[developer.pandosearch.com](https://developer.pandosearch.com/).

## Development

In development, just run `docker-compose up` and visit http://localhost:8000/
for a live-updating locally rendered HTML version of the documentation.

Under the hood, the [Dockerfile](./Dockerfile) installs necessary dependencies and
`mkdocs serve` is started for you. In [docker-compose.yml](./docker-compose.yml) we mount the project
directory and make the server available on port 8000.

See the source code for further details.
