# Keymatch

## What is keymatch?

The order of the Pandosearch search results is determined by the Pandosearch algorithm. Most of the time this works very well, but sometimes you want to override this. You can do this using the _keymatch_ feature.

Keymatch instructs Pandosearch to show a page as the top result for a certain search query.

## Using keymatch

You can activate keymatch using a `<meta>` tag in the `<head>` of your HTML. Multiple search queries can be entered comma-separated.

Example:

```html
<html>
  <head>
    <meta name="keymatch" content="weather, weather report"/>
    ...
  </head>
  <body>
    ...
  </body>
</html>
```

With the example above, website visitors will get this page as top result when searching for either "weather" or "weather report".

How you add the keymatch tag to your source HTML depends on how your website is managed, e.g. with a content management system (CMS) or other tooling.
