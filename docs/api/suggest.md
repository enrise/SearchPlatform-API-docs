# Suggest

With the suggest action, a collection is queried using a partial search term. This makes it ideal for live autocomplete and/or autosearch.

When processing a request, Pandosearch is using multiple fields for each document in the collection. The exact fields depend on the specific configuration for the collection.

## Request

To retrieve suggestions from Pandosearch, make a `GET` request to the following URL:

```sh
https://public.pandosearch.com/:collection/suggest
```

All possible parameters need to be provided as query parameters.

In the rest of this page, the full URL and collection name are omitted for readability.


### Parameter: `q`

Use the `q` query parameter when searching for suggestions based on a part of the full query string. The `q` parameter is automatically escaped and is always interpreted as a string.

The following call will return all suggestions and hits for "pand":

```sh
suggest?q=pand
```

For autocompletion, the _next_ word that is likely to be typed is often relevant. When you end the search query with a space, the next most likely word that is automatically returned in the suggestions.

Example: the following call will return suggestions for the next word after "what":

```sh
suggest?q=what%20
```

A couple of caveats apply:

* Be aware that a space is not a valid character in a URL and thus needs to be URL encoded (i.e. `%20`).
* Trailing whitespace is by default often considered irrelevant when processing form input data. To ensure Pandosearch correctly returns next words, please double-check that your form data processing logic does _not_ strip trailing whitespace from the `q` parameter.


### Parameter: `size`

To control the number of results returned for each request, use the `size` query parameter. The `size` parameter expects an integer and defaults to `5`.

Example: the following call will return 5 suggest results for "pand":

```sh
suggest?q=pand&size=5
```

### Parameter: `track`

In Pandosearch, all API requests are automatically tracked. We do not track any personal information – only information about the query sent and the results returned.

Tracking can be turned off by using the `track` parameter. If `track=false` is passed, the result won't be tracked or included in usage reports. In all other cases, the result will be tracked and included in usage reports.

An example use case is internal testing. You usually don't want your test requests to pollute your search usage analytics. To achieve this, you can use the `track` parameter as follows:

```sh
suggest?q=pand&track=false
```

## Response

A suggest response is a JSON document with the following structure:

```json
{
  "suggestions": [
    {
      "text": "pandosearch"
    },
    {
      "text": "panden"
    },
    {
      "text": "panda"
    }
  ],
  "hits": [
    {
      "title": "Search",
      "url": "https://developer.pandosearch.com/api/search/",
      "type": "page"
    },
    {
      "title": "Introduction",
      "url": "https://developer.pandosearch.com/api/",
      "type": "page"
    },
    {
      "title": "Suggest",
      "url": "https://developer.pandosearch.com/api/suggest/",
      "type": "page"
    }
  ],
  "timing": {
    "search": 13.26,
    "search:took": 12,
    "request": 19.63
  }
}
```

The suggest action returns three types of data:

- `suggestions` – words in the index that contain the given query string. These are ideal for an autocomplete implementation (to help the user finalize the query).
- `hits` – Documents that contains the given query string. These are ideal for an autosearch implementation (give the user search results based on "half a word").
- `timing` – Data that can be used for monitoring performance.

Read on for more details for each individual response data part.


### Data: `suggestions`

The `suggestions` property contains the words in the index that contain the given search query. These suggestions are ranked based on the occurence of that word in the index. The `suggestions` are always represented as array of objects. Each object looks as follows:

```json
{
  "text": "pandosearch"
}
```

#### Completions and predictions

The `suggestions` data technically consists of two types of suggestions:

- _completions_ – suggestions for words starting with the currently provided input.
- _predictions_ – suggestions for next words after the currently provided search term(s).

By default, Pandosearch returns _completions_ if it can find any. If not, predictions are returned. Predictions are always returned if the given `q` parameter ends with a space (" ").

Note that predictions can be disabled for individual implementations. If so, next words are never returned in the API response data, regardless of the input sent.

It is also possible to have predictions returned as a separate `predictions` response data field. This can be useful in case of avanced front-end data processing needs.

#### Highlighting

It is possible to use highlighting on suggestions. This option is enabled by us on request.

If enabled, the `display` property is appended for each suggestion. It highlights the query in the given suggestion.

The highlighting tags used are configurable. Example using `<b>` tags:

```json
{
  "text": "pandosearch",
  "display": "<b>pand</b>osearch"
}
```


### Data: `hits`

The `hits` property contains the full collection of documents found in the index, based on the given search query. It is always represented as array of objects. Each object looks as follows:

```json
{
  "title": "Search",
  "url": "https://developer.pandosearch.com/api/search/",
  "type": "page"
}
```

For each document, a range of fields can be returned. The fields that are returned differ per customer. Each field is highlighted by default by wrapping the search term in `<b></b>` tags. The `url` field is the unique identifier for this document. The `type` field is the type of document.

#### Highlighting

When returning results, each document is returned highlighted. This works as follows:

The part where the search term was found in a field of the document is transformed to a snippet. A snippet is (by default) max 250 characters long and it is "smart": words are kept intact as much as possible. The snippet is always created around the search term, so the search term will always be included in the snippet and is wrapped in `<b></b>` tags (or other markup as configured for your implementation). Per result, only one snippet is returned.


### Data: `timing`

The `timing` object contains all information about the time your request took. It is divided into:

- `search:took` – Total time spent on search query.
- `search` – Total time between sending the search query and receiving back a result (this is basically `search:took` + network overhead).
- `request` – Total amount of time between the request coming in and sending the response out.

All time durations are expressed in milliseconds.
