# Introduction

The Pandosearch API allows you to perform search requests against collections of indexed content. Possibilities include searching, filtering, sorting, autocomplete suggestions, and more.

## Terminology

Although we try to use plain language as much as possible, some specific terminology is used here and there.

- **Collection** – Indexed content which can be searched. A collection is usually identified by a domain-name-like key (`pandosearch.com`, `developer.pandosearch.com`), optionally suffixed by a language identifier (`-nl`, `-en`, etc.) or specific site section name (`-blog`, `-archive`, etc.). Content can originate from various sources, such as web pages, PDF documents, and video metadata.
- **Action** – An API action, usually on a specific collection.
- **Hits** – A set of documents (identified by URLs) found for a given search term.

## Authentication

The Pandosearch API requires no authentication by default. Reason for this is that Pandosearch is often used for searching through publicly available online content. This means that both the content we index and the search interface from where the Pandosearch API is called are public.

For specific customer needs, we do offer various authentication solutions. The technical details of those are beyond the scope of this documentation.

## Configuration

Every individual Pandosearch implementation has its own configuration settings. These settings can affect parts of the API responses returned. If so, the API documentation mentions this.

As these configuration settings are complex in nature, and sometimes very customer-specific, this documentation simply cannot cover all possible scenarios. Should anything be unclear, we are always happy to assist through the support channels available for your plan.

## Paths

All API URLs have the following structure:

```sh
https://public.pandosearch.com/:collection/:action
```

- `:collection` is the collection as defined for your implementation.
- `:action` is the specific action. Usually `search` or `suggest`.

E.g. for the `developer.pandosearch.com` collection, the `search` action is available at:

```sh
https://public.pandosearch.com/developer.pandosearch.com/search
```

For detailed action information, see the [Search](search.md), [Suggest](suggest.md) and [Recommendations](recommendations.md) pages.

Note that for specific customer needs, additional actions may be available. These are not included in this public version of our API documentation.

## Requests

All API endpoints are accessed using HTTP `GET`.

Request parameters have to be passed as URL parameters in a query string. Example:

```sh
https://public.pandosearch.com/developer.pandosearch.com/search?q=pandosearch&page=2
```

## Responses

API responses are always in [JSON](https://www.json.org/) format. Hence you can, but don't need to, provide the `Accept: application/json` header with your request.
