# Recommendations

The recommendations action returns related content for a given URL. A typical use case is to show similar content to a visitor (e.g. pages, products, blog posts) as an invitation to stay and discover more.

As Pandosearch is continually updating collections based on actual data, the recommendations returned will also automatically update if new content has been found that relates to the content on a given page.

Please note that recommendations are disabled by default. The API endpoint can be enabled as an add-on. Please contact support for more information on pricing and availability.

## Request

To retrieve recommendations from Pandosearch, make a `GET` request to the following URL:

```sh
https://public.pandosearch.com/:collection/recommendations
```

All possible parameters need to be provided as query parameters.

In the rest of this page, the full URL and collection name are omitted for readability.


### Parameter: `id`

Use a document URL as `id` query parameter to retrieve recommendations.

The following call will return recommendations for "https://developer.pandosearch.com/api":

```sh
recommendations?id=https%3A%2F%2Fdeveloper.pandosearch.com%2Fapi
```

As shown in the example, the `id` value needs to be [URL encoded](https://en.wikipedia.org/wiki/Percent-encoding).

### Parameter: `size`

To control the number of recommendations returned, use the `size` query parameter. The `size` parameter expects an integer and defaults to `5`.

Example: the following call will return 8 recommendations for "https://developer.pandosearch.com/api":

```sh
recommendations?id=https%3A%2F%2Fdeveloper.pandosearch.com%2Fapi&size=8
```

### Parameter: `track`

In Pandosearch, all API requests are automatically tracked. We do not track any personal information – only information about the query sent and the results returned.

Tracking can be turned off by using the `track` parameter. If `track=false` is passed, the result won't be tracked or included in usage reports. In all other cases, the result will be tracked and included in usage reports.

An example use case is internal testing. You usually don't want your test requests to pollute your search usage analytics. To achieve this, you can use the `track` parameter as follows:

```sh
recommendations?id=https%3A%2F%2Fdeveloper.pandosearch.com%2Fapi&track=false
```

## Response

A recommendations response is a JSON document with the following structure:

```json
{
  "total": 49,
  "hits": [
    {
      "type": "page",
      "url": "https://developer.pandosearch.com/api/search/",
      "fields": {
        "title": "Search",
        "description": "Search endpoint API documentation."
      }
    },
    {
      "type": "page",
      "url": "https://developer.pandosearch.com/api/suggest/",
      "fields": {
        "title": "Suggest",
        "description": "Suggest endpoint API documentation."
      }
    },
    {
      "type": "page",
      "url": "https://developer.pandosearch.com/api/recommendations/",
      "fields": {
        "title": "Recommendations",
        "description": "Recommendations endpoint API documentation."
      }
    },
    {
      "type": "blog",
      "url": "https://developer.pandosearch.com/guides/search-results/",
      "fields": {
        "title": "Search results",
        "description": "Search results page building guide."
      }
    }
  ],
  "request": {
    "id": "https://developer.pandosearch.com/api",
    "size": 5,
    "track": true
  },
  "received": {
    "id": "https://developer.pandosearch.com/api"
  },
  "timing": {
    "search": 19.65,
    "search:took": 18,
    "request": 21.29
  }
}

```

The following sections provide detailed information for each response data part.

### Data: `hits`

The `hits` property contains the recommendations found in the index. It is represented as array of objects. Each object looks as follows:

```json
{
  "type": "page",
  "url": "https://developer.pandosearch.com/api/search/",
  "fields": {
    "title": "Search",
    "description": "Search endpoint API documentation."
  }
}
```

Each property explained in more detail:

* `type` – the type of document. Available document types are specific for every implementation.
* `url` – the URL where the document can be found. Use this value when showing a hyperlink to visitors.
* `fields` – an object containing document data to display to visitors. This can be a title, body text, an image URL, or any other value known to Pandosearch. The exact fields returned are configured specifically for each implementation.


### Data: `request`

This is an object containing the full request object with which a request was performed in our API. It contains all query string parameters you provided and all default settings for other parameters.


### Data: `received`

This is an object containing only the sanitized query string parameters you provided.


### Data: `timing`

The `timing` object contains all information about the time your request took. It is divided into:

- `search:took` – Total time spent on search query.
- `search` – Total time between sending the search query and receiving back a result (this is basically `search:took` + network overhead).
- `request` – Total amount of time between the request coming in and sending the response out.

All time durations are expressed in milliseconds.
